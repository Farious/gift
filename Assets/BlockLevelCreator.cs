﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockLevelCreator : MonoBehaviour {
	private GameObject leftWall;
	private GameObject rightWall;

	private List<GameObject> blocks;
	private int maxBlocks;

	// Use this for initialization
	void Start () {
		leftWall = GameObject.Find("LeftWall");
		rightWall = GameObject.Find("RightWall");

		int nrBlocks = Mathf.FloorToInt(Random.value * 30) + 60;
		blocks = new List<GameObject>(nrBlocks);
		maxBlocks = Mathf.FloorToInt((rightWall.transform.position.x - leftWall.transform.position.x - 4)/2);

		for (int i = 0; i < nrBlocks; i++) {
			GameObject block = (GameObject)Instantiate(Resources.Load(RetrieveColor()));
			block.transform.position = RetrievePosition(i);
			block.AddComponent<BlockPhysics>();
			blocks.Add(block);
		}

	}

	string RetrieveColor(){
		float val = Random.value;
		if (val < 0.3333)
			return "Block_Orange";
		else if (val < 0.6666)
			return "Block_Red";
		else
			return "Block_Yellow";
	}

	Vector3 RetrievePosition(int i){
		return new Vector3(((i%maxBlocks) * 2) - 20 + 3 + 1,  7 - Mathf.Floor(i/maxBlocks), 0);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
