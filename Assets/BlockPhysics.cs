﻿	using UnityEngine;
using System.Collections;

public class BlockPhysics : MonoBehaviour {
	public int maxLife = 3;
	public LevelStatus lvlStatus;
	// Use this for initialization
	void Start () {
		lvlStatus = GameObject.Find("Main Camera").GetComponent<LevelStatus>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	void OnCollisionExit2D(Collision2D coll){
		if (coll.gameObject.tag == "Ball"){
			maxLife--;
			lvlStatus.AddPoints(10);
		}

		if (maxLife == 0){
			GameObject.Destroy(gameObject);
			lvlStatus.AddPoints(100);
		}
	}
}
