﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour {
	private LevelStatus lvlStatus;
	private dfSlicedSprite sprite;
	private Rigidbody2D body;
	private float yPos;
	private float amForce;

	// Use this for initialization
	void Start () {
		lvlStatus = GameObject.Find("Main Camera").GetComponent<LevelStatus>();
		body = rigidbody2D;
		yPos = transform.position.y;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 lockPosition = transform.position;
		lockPosition.y = yPos;
		transform.position = lockPosition;

		amForce = Input.GetAxis("Horizontal");
		body.velocity = new Vector2(amForce * 60, 0);

		Vector3 rot = new Vector3(0, 0, amForce * -15);

		Quaternion localRot = transform.localRotation;
		localRot.eulerAngles = rot;
		transform.localRotation = localRot;

		Vector3 relPos = transform.position;
		if ( transform.position.x > 19.5f - transform.localScale.x/2 - 0.25f){
			relPos.x = 19.5f - transform.localScale.x/2 - 0.25f;
		}else if(transform.position.x < -19.5f + transform.localScale.x/2 + 0.25f){
			relPos.x = -19.5f + transform.localScale.x/2 + 0.25f;
		}

		if ( relPos.y != -8.0f){
			relPos.y = -8.0f;
		}

		transform.position = relPos;

	}

	void OnCollisionExit2D(Collision2D coll){
		if (coll.gameObject.tag == "Ball"){
			coll.gameObject.rigidbody2D.AddForce(new Vector2(amForce * 100f, 20f));
		}
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.layer == LayerMask.NameToLayer("Candy")){
			if (coll.gameObject.name == "GreenBonus"){
				lvlStatus.AddPoints(1000);
			}else if (coll.gameObject.name == "RedBonus"){
				lvlStatus.AddPoints(2000);
			}

			GameObject.Destroy(coll.gameObject);
		}
	}
}
