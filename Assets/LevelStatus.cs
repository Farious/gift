﻿using UnityEngine;
using System.Collections;

public class LevelStatus : MonoBehaviour {
	public int points = 0;
	private dfLabel scoreBoard;
	private dfLabel multLbl;
	private float multTime;
	private float textTime;
	private int level = 0;
	private float lastMult;
	private float multiplier = 1.0f;
	private float lastTime;

	// Use this for initialization
	void Start () {
		multLbl = GameObject.Find("Multiplier").GetComponent<dfLabel>();
		scoreBoard = GameObject.Find("Points").GetComponent<dfLabel>();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateScoreboard();
		UpdateMultiplier();
		Bonus();
		Surprise();

		if (GameObject.FindGameObjectsWithTag("Heart").Length < 3000)
			CheckEnd();
	}

	void UpdateMultiplier(){
		multLbl.Text = multiplier.ToString() + " x Multiplier!!!";
	}

	void UpdateScoreboard(){
		string value = points.ToString();
		scoreBoard.Text = value;
	}

	public void AddPoints(int value){
		if (Time.time - lastTime < 1.0f){
			multiplier += 0.5f;
		}

		lastTime = Time.time;
		points += (int) (((float)value) * multiplier);
	}

	public void ResetMultiplier(){
		multiplier = 1.0f;
	}

	public void Bonus(){
		if (multiplier % 5f == 0 && lastMult != multiplier){
			GameObject bonus = (GameObject)Instantiate(Resources.Load(RandomBonus()));
			bonus.transform.position = new Vector3(Random.Range(-16, 16), 8, bonus.transform.position.z);
			lastMult = multiplier;
		}else if ( lastMult != multiplier){
			lastMult = 0;
		}
	}

	string RandomBonus(){
		float value = Random.value;

		if (value <= 0.333){
			return "RedBonus";
		}else if (value <= 0.666){
			return "GreenBonus";
		}else{
			return "Ball";
		}
	}

	void Surprise(){
		GameObject obj;
		dfLabel mile;
		dfTweenVector3 tween;
		if (points > 1000 && level == 0){
			obj = GameObject.Find("1stMilestone");
			mile = obj.GetComponent<dfLabel>();
			tween = obj.GetComponent<dfTweenVector3>();
			mile.IsVisible = true;
			tween.Play();
			level++;
			textTime = Time.time;
		}else if (points > 2000 && level == 1 && Time.time - textTime > 8){
			obj = GameObject.Find("2ndMilestone");
			mile = obj.GetComponent<dfLabel>();
			tween = obj.GetComponent<dfTweenVector3>();
			mile.IsVisible = true;
			tween.Play();
			level++;
			textTime = Time.time;
		}else if (points > 3000 && level == 2 && Time.time - textTime > 8){
			obj = GameObject.Find("3rdMilestone");
			mile = obj.GetComponent<dfLabel>();
			tween = obj.GetComponent<dfTweenVector3>();
			mile.IsVisible = true;
			tween.Play();
			level++;
			textTime = Time.time;
		}else if (points > 4000 && level == 3 && Time.time - textTime > 8){
			obj = GameObject.Find("4thMilestone");
			mile = obj.GetComponent<dfLabel>();
			tween = obj.GetComponent<dfTweenVector3>();
			mile.IsVisible = true;
			tween.Play();
			level++;
			textTime = Time.time;
		}else if (points > 5000 && level == 4 && Time.time - textTime > 8){
			obj = GameObject.Find("5thMilestone");
			mile = obj.GetComponent<dfLabel>();
			tween = obj.GetComponent<dfTweenVector3>();
			mile.IsVisible = true;
			tween.Play();
			level++;
			textTime = Time.time;
		}
	}

	void CheckEnd(){
		GameObject obj;
		dfLabel mile;
		dfTweenVector3 tween;
		if (GameObject.FindGameObjectsWithTag("Bricks").Length == 0 && level == 5 && Time.time - textTime > 8){
			obj = GameObject.Find("5thMilestone");
			mile = obj.GetComponent<dfLabel>();
			tween = obj.GetComponent<dfTweenVector3>();
			mile.IsVisible = true;
			tween.Play();
			level++;
			textTime = Time.time;
			GetComponent<AudioSource>().Play();
		}else if (GameObject.FindGameObjectsWithTag("Bricks").Length == 0 && level == 6 && Time.time - textTime > 8){
			GameObject bonus = (GameObject)Instantiate(Resources.Load("Heart"));
			bonus.transform.position = new Vector3(Random.Range(-16, 16), 8, bonus.transform.position.z);
		}
	}
}
