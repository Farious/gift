﻿using UnityEngine;
using System.Collections;

public class MenuAnimator : MonoBehaviour {

	public dfTiledSprite menuBg;
	public dfLabel title;
	public dfLabel pushStart;
	public dfPanel buttonArea;
	public dfButton playBtn;

	private bool onStart = true;
	private float totalTime = 0.0f;
	private float runTime = 2.0f;
	private int sign = 1;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		var tileX = menuBg.TileScroll;
		tileX.x += 0.01f;
		menuBg.TileScroll = tileX;

		if (onStart) {
			if (Time.timeSinceLevelLoad > runTime){
				float opacity = pushStart.Opacity;

				if (opacity > 0 && opacity < 1){
					sign = sign;
				}else{
					if ((opacity == 0 && sign == -1) || (opacity == 1 && sign == 1))
						sign = -1 * sign;
					else
						sign = sign;
				}

				opacity += (0.01f * sign);
				pushStart.Opacity = opacity;
			}
		}else{
			var titlePos = title.RelativePosition;

			if (titlePos.y <= 0){
				titlePos.y = 0;

				buttonArea.Enable();
				buttonArea.IsVisible = true;
			}else{
				titlePos.y -= 4f;
			}

			title.RelativePosition = titlePos;
		}

		if (Input.GetMouseButtonDown(0)){
			onStart = false;
			pushStart.enabled = false;
		}
	}
}
