﻿using UnityEngine;
using System.Collections;

public class SphereController : MonoBehaviour {
	private LevelStatus lvlStatus;
	public float maxVelocity = 8.0f;

	// Use this for initialization
	void Start () {
		lvlStatus = GameObject.Find("Main Camera").GetComponent<LevelStatus>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (rigidbody2D.velocity.magnitude > maxVelocity){
			rigidbody2D.velocity = rigidbody2D.velocity.normalized * maxVelocity;
		}
	}

	void OnCollisionExit2D(Collision2D coll){
		if (coll.gameObject.name == "Floor"){
			lvlStatus.ResetMultiplier();
		}
	}
}
